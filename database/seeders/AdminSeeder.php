<?php

namespace Database\Seeders;

use App\Models\MasterBarang;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $daftar_barang = ([
            ['nama_barang' => 'Sabun Batang',     'harga_satuan' => '3000', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => null],
            ['nama_barang' => 'Mi Instan',        'harga_satuan' => '2000', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => null],
            ['nama_barang' => 'Kopi Sachet',      'harga_satuan' => '1500', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => null],
            ['nama_barang' => 'Air Minum Galon',  'harga_satuan' => '20000', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => null],
        ]);
        foreach ($daftar_barang as $value) {
            MasterBarang::create($value);
        }
    }
}
