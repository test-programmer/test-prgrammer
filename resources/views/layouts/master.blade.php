<!DOCTYPE html>
<html lang="en" >
    <head>
        @include('layouts.component.head')
        <title>@stack('title')</title>
        @stack('script-head')
    </head>
    <body class="main">
        <div id="static-backdrop-modal-preview" class="modal overflow-y-auto show" data-backdrop="static" tabindex="-1" aria-hidden="false" style="margin-top: 0px; margin-left: 0px; padding-left: 0px; z-index: 10000;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body px-5 py-10">
                        <div class="text-center">
                            <i data-loading-icon="rings" class="w-16 h-16 text-theme-1 mx-auto mt-3"></i>
                            <div class=" mt-5 text-center ">Loading, please wait...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- END: Modal Content -->
        @include('layouts.component.mobile-menu')
        @include('layouts.component.top-bar')
        <div class="wrapper">
            <div class="wrapper-box">
                @include('layouts.component.side-menu')
                <div class="content">
                    <div class="relative">
                        <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
                            <h2 class="text-lg font-medium mr-auto">
                                @stack('name-content')
                            </h2>
                        </div>
                        @include('layouts.component.alert')
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.component.script')
        <!-- BEGIN: Modal Content -->
        @stack('script')
    </body>
</html>


